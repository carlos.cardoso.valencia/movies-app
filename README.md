# App "e Movie"

Esta aplicación consume la [API de TMDB ](https://www.themealdb.com/api.php) para presentar un listado de peliculas. Al dar clic en cada una se puede ver un detalle con más informacion de dicha pelicula, asi como reseñas de las mismas. La aplicacion tambien incluye otras funcionaliades que se explican con detalle más adelante.

La aplicación se realizó siguiendo una arquitectura *MVVM*,  *Hilt* para la inyección de dependencias, *Compose* para la definicion de la UI, *Retrofit* para el consumo de los servicios. *Room* para el almacenamiento de la data local.  Además, se construyó siguiendo las pautas de una app modular, constando de los siguientes modulos.

- :core:network
- :core:firebase
- :data:movie
- :data:images
- :data:location
- :features:
- app

### Grafo de dependencias de los modulos

![alt text](https://parsefiles.back4app.com/bb2RyYIiDCvjNveH9nocpd4X8d72H5mAyL5GSGrD/8cc7ad73de8152b8e282baeaf93ee7c1_Three-set%20Venn%20diagram.png)

### Modulo :core:network
Define las configuraciones de retrofit necesarias para realizar las peticiones web. Contiene los modelos de red y ejecuta los requests a las apis.

### Modulo :core:firebase
Integra las dependencias necesarias para usar las funcionalidades de Firebase "Firestore database y "Cloud firestore" y las operaciones de lectura y escritura hacia estas bases de datos.

### Modulo :data:movie
Presenta los repositorios que brindan acceso a los datos de las peliculas. Realiza el mapeo de los datos de modelos de red a los modelos que requiere la UI.

### Modulo :data:location
Incluye los repositorios que guardan los datos de la locacion del usuario en Firestore y expone metodos para obtener dichos elementos y poderse presntar en la UI en un listado y en un mapa

### Modulo :data:images
Incluye los repositorios que interactuan con Cloud firestore para guardar y obtener las imagenes del usuario

### Modulo app
Modulo principal que define todo el grafo del flujo de navegacion de la app.

### Modulo :features:
Contiene las funcionalidades propias de esta aplicacion, las cuales se describen a continuación

### Feature 1: Listado de peliculas

En esta sección se presenta un listado de peliculas al usuario obtenidas de la [API de TMDB ](https://www.themealdb.com/api.php). Se muestran separadas por las categorias "Mas populares", "Recomendadas" y "Mejor calificadas". En caso de no tener internet el usuario o haber una error en las peticiones, eesta sección está preprada para presentar un lista de peliculas tomado de la base de datos local, ya que previamente en cada descarga se almacena en room el listado de peliculas.

Al dar clic en una pelicula se hace una peticion a *getMovieDetail* para obtener mas detalle de la pelicula, como el listado de generos o un listado de reseñas. De igual manera,en caso de error de conexion se obtiene de room el detalle que tenga almacenada la aplicación.

![alt text](https://parsefiles.back4app.com/bb2RyYIiDCvjNveH9nocpd4X8d72H5mAyL5GSGrD/50e861376479aa0fef14bbb83a17e53d_descarga.png)

### Feature 2: Listado de actores populares

Esta seccion de la app presenta un listado de las personas populares de acuerdo a como lo devuelve la api de TMDB. Esta seccion no cuenta con logica para almacenamiento de datos locales

![alt text](https://parsefiles.back4app.com/bb2RyYIiDCvjNveH9nocpd4X8d72H5mAyL5GSGrD/fe86fe95c176a141b83a6c32f7d437eb_descarga2.png)

### Feature 2: Tracking de la ubicacion del usuario
Mediante un servicio que corre en background se obtiene la ubicacion del usuario y se guardan las coordenadas en Firestore. El proceso se repite cada 5 minutos. Se notifica al usuario mediante una push local cada que esto ocurre, y los datos se utilizan para presentarlos en un mapa y en listado como se muestra a continuación.

![alt text](https://parsefiles.back4app.com/bb2RyYIiDCvjNveH9nocpd4X8d72H5mAyL5GSGrD/83c686a21cec7d95d5f187cd33ee8738_location2.png)

### Feature 3: Subir imagenes a Cloud Firestore

En esta pantalla el usuario puede seleccionar de la galeria una o mas imagenes, que se subiran a Cloude Firestore. Se guardan las URLs en firestore para obtenerlas con mayor facilidad.

![alt text](https://parsefiles.back4app.com/bb2RyYIiDCvjNveH9nocpd4X8d72H5mAyL5GSGrD/8d588231a6daa639ee6f9b0938b0f68e_foo.png)


### ¿Que añadiria o que mejoraria al proyecto?

En cuanto a funcionalidades de las seccion de peliculas añadiria una barra de busqueda para que el usuario pueda encontrar mas facilmente alguna pelicula que tenga en mente. Añadiria la lógica para soportar la paginación, tanto del servicio como de room. Mejoraria la UI de las reviews y añadiria pruebas unitarias con *mockk*. 












