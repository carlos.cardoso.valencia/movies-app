package com.carlos.movies

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.compose.rememberNavController
import com.carlos.movies.navigation.BottomNavigationBar
import com.carlos.movies.navigation.BottomNavigationItem
import com.carlos.movies.navigation.NavigationHost
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        setContent {
            val navController = rememberNavController()

            val bottomNavigationItems = listOf(
                BottomNavigationItem.Profile,
                BottomNavigationItem.Movies,
                BottomNavigationItem.Locations,
                BottomNavigationItem.Images
            )
            Scaffold(
                bottomBar = {
                    BottomNavigationBar(navController, bottomNavigationItems)
                },
                content = { padding ->
                    NavigationHost(navController, Modifier.padding(padding))
                }
            )
        }
    }

}


