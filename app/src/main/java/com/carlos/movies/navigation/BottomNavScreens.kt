package com.carlos.movies.navigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Image
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.carlos.movies.R


sealed class BottomNavigationItem(val route: String, @StringRes val resourceId: Int, val icon: ImageVector) {
    object Profile : BottomNavigationItem(
        route = "Profile",
        resourceId = R.string.label_bottom_nav_profile,
        icon = Icons.Filled.Person
    )

    object Movies : BottomNavigationItem(
        route = "Movies",
        resourceId = R.string.label_bottom_nav_movie,
        icon = Icons.Filled.Movie
    )

    object Locations : BottomNavigationItem(
        route = "Locations",
        resourceId = R.string.label_bottom_nav_location,
        icon = Icons.Filled.LocationOn
    )
    object Images : BottomNavigationItem(
        route = "Images",
        resourceId = R.string.label_bottom_nav_images,
        icon = Icons.Filled.Image
    )
}