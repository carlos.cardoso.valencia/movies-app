package com.carlos.movies.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.carlos.features.detail.DetailScreen
import com.carlos.features.detail.ReviewsScreen
import com.carlos.features.image.ImagesScreen
import com.carlos.features.location.LocationScreen
import com.carlos.features.movies.MoviesScreen
import com.carlos.features.person.PersonsScreen
import com.carlos.movies.navigation.DestinationsArgs.MOVIE_ID_KEY

@Composable
fun NavigationHost(navController: NavHostController, modifier: Modifier) {

    NavHost(
        navController,
        startDestination = BottomNavigationItem.Movies.route,
        modifier = modifier
    ) {
        composable(BottomNavigationItem.Movies.route) {
            MoviesScreen(onItemClick = { movieId ->
                navController.navigate("${Routes.Detail.name}?${MOVIE_ID_KEY}=${movieId}")
            })
        }

        composable(BottomNavigationItem.Profile.route) {
            PersonsScreen()
        }

        composable(BottomNavigationItem.Locations.route) {
            LocationScreen()
        }

        composable(BottomNavigationItem.Images.route) {
            ImagesScreen()
        }

        composable(
            route = "${Routes.Detail.name}?${MOVIE_ID_KEY}={$MOVIE_ID_KEY}",
            arguments = listOf(
                navArgument(MOVIE_ID_KEY) {
                    type = NavType.StringType
                    nullable = true
                },
            )
        ) { entry ->
            val movieId = entry.arguments?.getString(MOVIE_ID_KEY) ?: ""


            DetailScreen(movieId, onBackCallback = {
                navController.navigateUp()
            }) {
                navController.navigate("${Routes.Reviews.name}?${MOVIE_ID_KEY}=${movieId}")
            }
        }

        composable(
            route = "${Routes.Reviews.name}?${MOVIE_ID_KEY}={$MOVIE_ID_KEY}",
            arguments = listOf(
                navArgument(MOVIE_ID_KEY) {
                    type = NavType.StringType
                    nullable = true
                },
            )
        ) { entry ->
            val movieId = entry.arguments?.getString(MOVIE_ID_KEY) ?: ""

            ReviewsScreen(movieId) {
                navController.navigateUp()
            }

        }

    }
}