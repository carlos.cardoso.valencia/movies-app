package com.carlos.firebase.di

import com.carlos.firebase.service.CloudStorageService
import com.carlos.firebase.service.CloudStorageServiceImpl
import com.carlos.firebase.service.LocationFirestoreService
import com.carlos.firebase.service.LocationFirestoreServiceImpl
import com.google.firebase.Firebase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.storage
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface LocationModule {
    @Binds
    fun bindFirestoreLocation(service: LocationFirestoreServiceImpl): LocationFirestoreService

    @Binds
    fun bindFirestoreStorage(service: CloudStorageServiceImpl): CloudStorageService


}


@Module
@InstallIn(SingletonComponent::class)
object DbModule {

    @Provides
    @Singleton
    fun providesFirestore(): FirebaseFirestore = Firebase.firestore

    @Provides
    @Singleton
    fun providesStorage(): FirebaseStorage = Firebase.storage


}