package com.carlos.firebase.model

import java.util.Date


data class ImageFirestoreModel(
    val url: String,
    val date: Date

) {
    constructor() : this("", Date())
}
