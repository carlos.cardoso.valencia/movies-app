package com.carlos.firebase.model

import java.util.Date

data class LocationFirestoreModel(
    val lat: Double,
    val lng: Double,
    val date: Date
) {

    constructor() : this(0.0, 0.0, Date())

}