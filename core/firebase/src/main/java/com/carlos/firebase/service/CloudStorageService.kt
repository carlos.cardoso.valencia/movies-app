package com.carlos.firebase.service

import android.graphics.Bitmap
import com.carlos.firebase.model.ImageFirestoreModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.util.Date
import javax.inject.Inject

interface CloudStorageService {
    fun uploadImage(bitmap: Bitmap): Flow<Boolean>
    fun getImages(): Flow<List<String>>

}

class CloudStorageServiceImpl @Inject constructor(
    private val storage: FirebaseStorage,
    private val db: FirebaseFirestore
) : CloudStorageService {

    private val FOLDER_NAME = "images"
    private val IMAGE_COLLECTION_NAME = "images"
    override fun uploadImage(bitmap: Bitmap) = callbackFlow {
        val storageRef = storage.reference
        val imagesRef = storageRef.child(FOLDER_NAME)
        val fileName = Date().time.toString()
        val imageRef = imagesRef.child("$fileName.jpg")

        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = imageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            launch { send(false) }

        }.addOnSuccessListener { taskSnapshot ->

            taskSnapshot.storage.downloadUrl.addOnSuccessListener { uri ->
                val imageFirestoreModel = ImageFirestoreModel(
                    url = uri.toString(),
                    date = Date()
                )
                db.collection(IMAGE_COLLECTION_NAME)
                    .add(imageFirestoreModel)
                    .addOnSuccessListener {}
                    .addOnFailureListener {}

                launch { send(true) }
            }
        }

        awaitClose { }
    }

    override fun getImages() = callbackFlow {
        val eventDocument = db.collection(IMAGE_COLLECTION_NAME)

        val subscription = eventDocument.addSnapshotListener { snapshot, exception ->
            snapshot?.let {
                if (!snapshot.isEmpty) {
                    launch {
                        val images =
                            snapshot.documents.map { it.toObject(ImageFirestoreModel::class.java) }
                        send(images.sortedBy { it?.date }.map { it?.url ?: "" })
                    }
                } else {
                    launch { send(emptyList<String>()) }
                }
            } ?: launch {
                send(emptyList<String>())
            }
        }

        awaitClose { subscription.remove() }
    }

}

