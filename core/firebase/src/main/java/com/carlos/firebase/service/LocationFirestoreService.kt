package com.carlos.firebase.service

import com.carlos.firebase.model.LocationFirestoreModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


interface LocationFirestoreService {
    fun saveLocation(location: LocationFirestoreModel)
    fun getLocations(): Flow<List<LocationFirestoreModel?>>
}

class LocationFirestoreServiceImpl @Inject constructor(
    private val db: FirebaseFirestore
) : LocationFirestoreService {

    private val COLLECTION_NAME = "locations"
    override fun saveLocation(location: LocationFirestoreModel) {
        db.collection(COLLECTION_NAME)
            .add(location)
            .addOnSuccessListener {}
            .addOnFailureListener {}
    }

    override fun getLocations(): Flow<List<LocationFirestoreModel?>> = callbackFlow {
        val eventDocument = db.collection(COLLECTION_NAME)

        val subscription = eventDocument.addSnapshotListener { snapshot, exception ->
            snapshot?.let {
                if (!snapshot.isEmpty) {
                    launch {
                        val locations =
                            snapshot.documents.map { it.toObject(LocationFirestoreModel::class.java) }
                        send(locations)
                    }
                }
            }
        }

        awaitClose { subscription.remove() }

    }


}