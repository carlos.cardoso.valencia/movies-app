package com.carlos.network

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException


class NetworkCall {

    sealed class Response<out T> {
        data class Success<out T>(val data: T) : Response<T>()
        data class GenericError(val code: Int? = null, val error: String? = null) : Response<Nothing>()

        object NetworkError : Response<Nothing>()
    }

    suspend fun <T> performCall(
        dispatcher: CoroutineDispatcher,
        apiCall: suspend () -> T
    ): Response<T> {
        return withContext(dispatcher) {
            try {
                Response.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> Response.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        val errorResponse = throwable.toString()
                        Response.GenericError(code, errorResponse)
                    }

                    else -> {
                        Response.GenericError(-1, throwable.localizedMessage)
                    }
                }
            }
        }
    }

}
