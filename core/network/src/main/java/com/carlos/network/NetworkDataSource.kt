package com.carlos.network

import com.carlos.network.model.MoviesResponse


interface NetworkDataSource {
    suspend fun getMovies(): MoviesResponse

}