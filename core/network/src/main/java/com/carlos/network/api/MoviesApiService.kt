package com.carlos.network.api

import com.carlos.network.BuildConfig
import com.carlos.network.model.MovieNetworkModel
import com.carlos.network.model.MoviesResponse
import com.carlos.network.model.PersonsResponse
import com.carlos.network.model.ReviewsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MoviesApiService {

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey,
        @Query("page") page: Int = 1
    ): MoviesResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): MoviesResponse

    @GET("movie/upcoming")
    suspend fun getRecommendationsMovies(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): MoviesResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): MovieNetworkModel

    @GET("person/popular")
    suspend fun getPopularPersons(
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey,
    ): PersonsResponse

    @GET("movie/{movie_id}/reviews")
    suspend fun getMovieReviews(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String = BuildConfig.TMDBApiKey
    ): ReviewsResponse


}