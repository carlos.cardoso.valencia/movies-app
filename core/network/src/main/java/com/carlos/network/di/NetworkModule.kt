package com.carlos.network.di

import com.carlos.network.NetworkCall
import com.carlos.network.RetrofitClient
import com.carlos.network.api.MoviesApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton



@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun providesNetworkCall(): NetworkCall = NetworkCall()

    @Provides
    @Singleton
    fun providesApiService(): MoviesApiService = RetrofitClient.createService(MoviesApiService::class.java)


}