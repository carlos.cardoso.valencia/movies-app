package com.carlos.images.di

import com.carlos.images.repository.ImagesRepository
import com.carlos.images.repository.ImagesRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
interface ImagesModule {
    @Binds
    fun bindsImagesRepository(repo: ImagesRepositoryImpl): ImagesRepository

}