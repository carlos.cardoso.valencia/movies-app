package com.carlos.images.repository

import android.graphics.Bitmap
import com.carlos.firebase.service.CloudStorageService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface ImagesRepository {
    fun uploadImage(bitmap: Bitmap): Flow<Boolean>
    fun getImages(): Flow<List<String>>
}

class ImagesRepositoryImpl @Inject constructor(
    private val service: CloudStorageService
) : ImagesRepository {

    override fun uploadImage(bitmap: Bitmap) = service.uploadImage(bitmap)

    override fun getImages() = service.getImages()

}