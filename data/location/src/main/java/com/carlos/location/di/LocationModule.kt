package com.carlos.location.di

import com.carlos.location.repository.LocationRepository
import com.carlos.location.repository.LocationRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
interface LocationModule {
    @Binds
    fun bindLocationRepository(repo: LocationRepositoryImpl): LocationRepository

}
