package com.carlos.location.mapper

import com.carlos.firebase.model.LocationFirestoreModel
import com.carlos.location.model.LocationUIModel
import java.util.Date

fun LocationUIModel.toFirestoreModel(): LocationFirestoreModel {
    return LocationFirestoreModel(
        lat = lat,
        lng = lng,
        date = date
    )
}


fun List<LocationFirestoreModel?>.mapFromFirestoreModel(): List<LocationUIModel> {
    return map { movie ->
        with(movie) {
            LocationUIModel(
                lat = this?.lat ?: 0.0,
                lng = this?.lng ?: 0.0,
                date = this?.date ?: Date()
            )
        }

    }
}