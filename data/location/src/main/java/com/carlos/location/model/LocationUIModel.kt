package com.carlos.location.model

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


data class LocationUIModel(
    val lat: Double,
    val lng: Double,
    val date: Date
) {
    val formattedDate: String
        get() {
            val format = SimpleDateFormat("dd/MM/yyyy | HH:mm:ss", Locale.getDefault())
            return format.format(date)
        }

}