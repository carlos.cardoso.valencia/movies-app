package com.carlos.location.repository

import com.carlos.firebase.service.LocationFirestoreService
import com.carlos.location.mapper.mapFromFirestoreModel
import com.carlos.location.mapper.toFirestoreModel
import com.carlos.location.model.LocationUIModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject


interface LocationRepository {
    fun saveLocation(location: LocationUIModel)
    fun getLocations(): Flow<List<LocationUIModel>>
}

class LocationRepositoryImpl @Inject constructor(
    private val service: LocationFirestoreService
) : LocationRepository {

    override fun saveLocation(location: LocationUIModel) {
        service.saveLocation(location.toFirestoreModel())
    }

    override fun getLocations(): Flow<List<LocationUIModel>> {
        return service.getLocations().map {
            it.mapFromFirestoreModel().sortedBy { it.date }
        }

    }

}

