package com.carlos.movies.di

import android.content.Context
import com.carlos.movies.repository.LocalMovieRepositoryImpl
import com.carlos.movies.repository.LocalMoviesRepository
import com.carlos.movies.repository.PersonsRepository
import com.carlos.movies.repository.PersonsRepositoryImpl
import com.carlos.movies.repository.RemoteMoviesRepository
import com.carlos.movies.repository.RemoteMoviesRepositoryImpl
import com.carlos.movies.room.MovieDatabase
import com.carlos.movies.room.MoviesDao
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
interface DataModule {
    @Binds
    fun bindsRemoteMovieRepository(repo: RemoteMoviesRepositoryImpl): RemoteMoviesRepository

    @Binds
    fun bindsLocalMovieRepository(repo: LocalMovieRepositoryImpl): LocalMoviesRepository

    @Binds
    fun bindPersonsRepository(repo: PersonsRepositoryImpl): PersonsRepository

}

@Module
@InstallIn(SingletonComponent::class)
object DbModule {

    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext appContext: Context): MovieDatabase =
        MovieDatabase(appContext)


    @Provides
    fun provideMoviesDao(appDatabase: MovieDatabase): MoviesDao {
        return appDatabase.movieDao()
    }

}

