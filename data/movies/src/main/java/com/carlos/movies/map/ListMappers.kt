package com.carlos.movies.map

import com.carlos.movies.model.MovieRoomModel
import com.carlos.movies.model.MovieUIModel
import com.carlos.movies.model.PersonUIModel
import com.carlos.movies.model.ReviewMovieUIModel
import com.carlos.network.model.MovieNetworkModel
import com.carlos.network.model.PersonNetworkModel
import com.carlos.network.model.ReviewNetworkModel
import java.util.Date

fun List<MovieNetworkModel>.map(): List<MovieUIModel> {
    return map { movie ->
        with(movie) {
            MovieUIModel(
                id = id.toString(),
                title = originalTitle.orEmpty(),
                overview = overview.orEmpty(),
                posterPath = posterPath.orEmpty(),
                backdropPath = backdropPath.orEmpty(),
                language = originalLanguage.orEmpty(),
                voteAverage = voteAverage ?: 0.0,
                releaseDate = releaseDate ?: Date(),
                voteCount = voteCount ?: 0,
            )
        }

    }
}


fun List<MovieRoomModel>.mapFromRoom(): List<MovieUIModel> {
    return map { movie ->
        with(movie) {
            MovieUIModel(
                id = id.toString(),
                title = title.orEmpty(),
                overview = overview.orEmpty(),
                posterPath = posterPath.orEmpty(),
                backdropPath = backdropPath.orEmpty(),
                language = language.orEmpty(),
                voteAverage = voteAverage ?: 0.0,
                releaseDate = releaseDate ?: Date()
            )
        }

    }
}

fun List<PersonNetworkModel>.mapPersonsFromNetwork(): List<PersonUIModel> {
    return map { person ->
        with(person) {
            PersonUIModel(
                name = name,
                imageUrl = profile_path,
                popularity = popularity
            )
        }

    }
}

fun List<ReviewNetworkModel>.mapReviewsFromNetwork(): List<ReviewMovieUIModel> {
    return map { person ->
        with(person) {
            ReviewMovieUIModel(
                authorName = person.author,
                userPicturePath = person.author_details.avatar_path ?: "",
                content = content,
            )
        }

    }
}

