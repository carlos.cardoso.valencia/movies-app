package com.carlos.movies.map

import com.carlos.movies.model.MovieRoomModel
import com.carlos.movies.model.MovieUIModel
import com.carlos.network.model.MovieNetworkModel
import java.util.Date


fun MovieUIModel.toRoomModel() = MovieRoomModel(
    databaseId = id,
    title = title,
    overview = overview,
    posterPath = posterPath,
    backdropPath = backdropPath,
    language = language,
    voteAverage = voteAverage,
    releaseDate = releaseDate
)


fun MovieRoomModel.mapFromRoom() = MovieUIModel(
    id = databaseId.toString(),
    title = title.orEmpty(),
    overview = overview.orEmpty(),
    posterPath = posterPath.orEmpty(),
    backdropPath = backdropPath.orEmpty(),
    language = language.orEmpty(),
    voteAverage = voteAverage ?: 0.0,
    releaseDate = releaseDate ?: Date()
)


fun MovieNetworkModel.mapFromNetwork() = MovieUIModel(
    id = id.toString(),
    title = originalTitle.orEmpty(),
    overview = overview.orEmpty(),
    posterPath = posterPath.orEmpty(),
    backdropPath = backdropPath.orEmpty(),
    language = originalLanguage.orEmpty(),
    voteAverage = voteAverage ?: 0.0,
    releaseDate = releaseDate ?: Date(),
    voteCount = voteCount ?: 0,
    genres = genres?.take(3)?.map { it.name } ?: emptyList()
)

