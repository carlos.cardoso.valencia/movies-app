package com.carlos.movies.model



enum class MovieListType {
    Popular,
    TopRated,
    Recommendation
}