package com.carlos.movies.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date


@Entity(tableName = "movies")
data class MovieRoomModel(
    @PrimaryKey var id: Int? = null,
    var databaseId: String? = null,
    var title: String? = null,
    var overview: String? = null,
    var posterPath: String? = null,
    var backdropPath: String? = null,
    var language: String? = null,
    var releaseDate: Date? = null,
    var voteAverage: Double? = null,
    var type: String? = null
)