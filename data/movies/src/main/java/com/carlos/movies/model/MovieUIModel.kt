package com.carlos.movies.model

import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Date


data class MovieUIModel(
    val id: String = "",
    val title: String = "",
    val overview: String = "",
    val posterPath: String = "",
    val backdropPath: String = "",
    val language: String = "",
    val releaseDate: Date = Date(),
    val voteAverage: Double = 0.0,
    val voteCount: Int = 0,
    val genres: List<String> = emptyList()
)  {

    val year = releaseDate.year().toString()

    // - images (poster and backdrop)
    private val IMAGE_URL = "https://image.tmdb.org/t/p"

    fun getVoteFormatted(): String {
        val df = DecimalFormat("#.#")
        return df.format(voteAverage)
    }

    fun getPosterUrl(size: String = "400") = "$IMAGE_URL/w$size/$posterPath"
    fun getBackDropUrl(size: String = "500") = "$IMAGE_URL/w$size/$backdropPath"
}


// TODO: Move to utils module
fun Date.year(): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar.get(Calendar.YEAR)

}