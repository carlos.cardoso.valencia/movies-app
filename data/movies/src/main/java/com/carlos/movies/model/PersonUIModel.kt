package com.carlos.movies.model

data class PersonUIModel(
    val name: String,
    val imageUrl: String,
    val popularity: Double
) {

    private val IMAGE_URL = "https://image.tmdb.org/t/p"
    fun getProfilePictureUrl(size: String = "400") = "$IMAGE_URL/w$size/$imageUrl"

}