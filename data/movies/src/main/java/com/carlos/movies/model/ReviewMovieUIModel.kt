package com.carlos.movies.model


data class ReviewMovieUIModel(
    val authorName: String = "",
    val userPicturePath: String = "",
    val content: String = "",
) {

    private val IMAGE_URL = "https://image.tmdb.org/t/p"
    fun getProfilepictureUrl(size: String = "400"): String {
        return if (userPicturePath.isEmpty()) {
            "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        } else {
            "$IMAGE_URL/w$size/$userPicturePath"
        }
    }

}

