package com.carlos.movies.repository

import com.carlos.movies.map.mapFromRoom
import com.carlos.movies.model.MovieRoomModel
import com.carlos.movies.model.MovieUIModel
import com.carlos.movies.room.MovieDatabase
import javax.inject.Inject


interface LocalMoviesRepository {
    suspend fun getPopularMovies(): List<MovieUIModel>
    suspend fun getTopRatedMovies(): List<MovieUIModel>
    suspend fun getRecommendatedMovies(): List<MovieUIModel>
    suspend fun insertMovie(movie: MovieRoomModel)
    suspend fun getMovieById(id: String): MovieUIModel

    suspend fun withDataInLocalStorage(): Boolean
}

class LocalMovieRepositoryImpl @Inject constructor(
    private val db: MovieDatabase
) : LocalMoviesRepository {

    /** */
    override suspend fun getPopularMovies() = db.movieDao().getPopular().mapFromRoom()

    /** */
    override suspend fun getRecommendatedMovies() = db.movieDao().getRecommended().mapFromRoom()

    /** */
    override suspend fun getTopRatedMovies() = db.movieDao().getTopRated().mapFromRoom()

    override suspend fun getMovieById(id: String) = db.movieDao().getMovieById(id).mapFromRoom()

    /** */
    override suspend fun insertMovie(movie: MovieRoomModel) {
        db.movieDao().insertMovie(movie)
    }

    /** */
    override suspend fun withDataInLocalStorage() = db.movieDao().getPopular().isNotEmpty()


}