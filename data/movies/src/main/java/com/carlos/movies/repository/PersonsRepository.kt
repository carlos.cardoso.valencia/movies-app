package com.carlos.movies.repository

import com.carlos.network.NetworkCall
import com.carlos.network.NetworkCall.Response
import com.carlos.network.api.MoviesApiService
import com.carlos.network.model.PersonsResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

interface PersonsRepository {
    suspend fun getPopularPersons(): Response<PersonsResponse>
}

class PersonsRepositoryImpl @Inject constructor(
    private val networkCall: NetworkCall,
    private val apiService: MoviesApiService
) : PersonsRepository {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO


    override suspend fun getPopularPersons() = networkCall.performCall(dispatcher) {
        apiService.getPopularPersons()
    }
}