package com.carlos.movies.repository

import com.carlos.network.NetworkCall
import com.carlos.network.NetworkCall.Response
import com.carlos.network.api.MoviesApiService
import com.carlos.network.model.MovieNetworkModel
import com.carlos.network.model.MoviesResponse
import com.carlos.network.model.ReviewsResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


interface RemoteMoviesRepository {
    suspend fun getPopularMovies(page: Int): Response<MoviesResponse>
    suspend fun getTopRatedMovies(): Response<MoviesResponse>
    suspend fun getRecommendatedMovies(): Response<MoviesResponse>

    suspend fun getMovieById(movieId: String): Response<MovieNetworkModel>
    suspend fun getMovieReviews(movieId: String): Response<ReviewsResponse>

}

class RemoteMoviesRepositoryImpl @Inject constructor(
    private val networkCall: NetworkCall,
    private val apiService: MoviesApiService
) : RemoteMoviesRepository {

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getPopularMovies(page: Int) = networkCall.performCall(dispatcher) {
        apiService.getPopularMovies(page = page)
    }

    override suspend fun getTopRatedMovies() = networkCall.performCall(dispatcher) {
        apiService.getTopRatedMovies()
    }

    override suspend fun getRecommendatedMovies() = networkCall.performCall(dispatcher) {
        apiService.getRecommendationsMovies()
    }

    override suspend fun getMovieById(movieId: String) = networkCall.performCall(dispatcher) {
        apiService.getMovieDetail(movieId)
    }

    override suspend fun getMovieReviews(movieId: String) = networkCall.performCall(dispatcher) {
        apiService.getMovieReviews(movieId)
    }
}