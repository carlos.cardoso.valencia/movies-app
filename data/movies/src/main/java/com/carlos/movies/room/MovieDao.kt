package com.carlos.movies.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.carlos.movies.model.MovieRoomModel


@Dao
interface MoviesDao {

    @Query("SELECT * FROM movies WHERE type = 'Popular' ")
    suspend fun getPopular(): List<MovieRoomModel>

    @Query("SELECT * FROM movies WHERE type = 'Recommendation' ")
    suspend fun getRecommended(): List<MovieRoomModel>

    @Query("SELECT * FROM movies WHERE type = 'TopRated' ")
    suspend fun getTopRated(): List<MovieRoomModel>

    @Query("SELECT * FROM movies WHERE id=:id ")
    fun getMovieById(id: String): MovieRoomModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: MovieRoomModel)


}