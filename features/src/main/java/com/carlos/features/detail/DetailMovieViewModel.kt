package com.carlos.features.detail


import androidx.lifecycle.ViewModel
import com.carlos.movies.map.mapFromNetwork
import com.carlos.movies.map.mapReviewsFromNetwork
import com.carlos.movies.model.MovieUIModel
import com.carlos.movies.model.ReviewMovieUIModel
import com.carlos.movies.repository.LocalMoviesRepository
import com.carlos.movies.repository.RemoteMoviesRepository
import com.carlos.network.NetworkCall
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val localRepository: LocalMoviesRepository,
    private val remoteRepository: RemoteMoviesRepository
) : ViewModel() {

    val isLoading = MutableStateFlow(false)
    val movie: MutableStateFlow<MovieUIModel> = MutableStateFlow(MovieUIModel())
    val reviews: MutableStateFlow<List<ReviewMovieUIModel>> = MutableStateFlow(emptyList())

    fun getMovie(id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            isLoading.emit(true)

            val response = remoteRepository.getMovieById(id)

            when (response) {
                is NetworkCall.Response.GenericError, NetworkCall.Response.NetworkError -> {
                    movie.emit(localRepository.getMovieById(id))
                }

                is NetworkCall.Response.Success -> {
                    movie.emit(response.data.mapFromNetwork())
                }
            }


        }
    }

    fun getReviews(movieId: String) {
        CoroutineScope(Dispatchers.IO).launch {

            val response = remoteRepository.getMovieReviews(movieId)

            when (response) {
                is NetworkCall.Response.GenericError, NetworkCall.Response.NetworkError -> {

                }

                is NetworkCall.Response.Success -> {
                    reviews.value = response.data.results.mapReviewsFromNetwork()
                }
            }


        }
    }

}

