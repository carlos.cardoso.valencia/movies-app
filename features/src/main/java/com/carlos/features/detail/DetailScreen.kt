package com.carlos.features.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.carlos.features.ui.BackArrow
import com.carlos.features.ui.RatingBar
import com.carlos.movies.model.MovieUIModel

@Composable
fun DetailScreen(
    movieId: String,
    viewModel: DetailMovieViewModel = hiltViewModel(),
    onBackCallback: () -> Unit,
    onReviewsClicked: (movieId: String) -> Unit
) {

    val movie by viewModel.movie.collectAsStateWithLifecycle()

    LaunchedEffect(viewModel) {
        viewModel.getMovie(movieId)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        Header(movie, modifier = Modifier.weight(0.55f), onBackCallback)

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Black)
                .padding(8.dp)
                .weight(0.45f)
        ) {

            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = movie.title,
                    fontSize = 30.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                )

                Text(
                    text = movie.language.uppercase(),
                    fontSize = 10.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .clip(RoundedCornerShape(corner = CornerSize(10.dp)))
                        .background(Color.White)
                        .padding(8.dp)
                )
            }

            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = "${movie.year} | ",
                    fontSize = 25.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                )

                Text(
                    text = "Reviews",
                    fontSize = 10.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .clip(RoundedCornerShape(corner = CornerSize(10.dp)))
                        .background(Color.White)
                        .padding(8.dp)
                        .clickable { onReviewsClicked.invoke(movieId) }
                )
            }




            ChipGenres(movie.genres, modifier = Modifier.padding(vertical = 8.dp))

            Text(
                text = movie.overview,
                fontSize = 18.sp,
                color = Color.White,
            )

        }


    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
private fun Header(
    movie: MovieUIModel,
    modifier: Modifier = Modifier,
    onBack: () -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .background(Color.Gray)
    ) {
        GlideImage(
            movie.getBackDropUrl(),
            contentDescription = "",
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxWidth()
        )

        BackArrow(
            onBack, modifier = Modifier.align(Alignment.TopStart)
        )

        Row(
            modifier = Modifier
                .padding(16.dp)
                .clip(RoundedCornerShape(corner = CornerSize(10.dp)))
                .background(Color.Black)
                .align(Alignment.BottomEnd)
                .padding(8.dp)
                .align(Alignment.Center)
        ) {
            RatingBar(rating = 1.0, stars = 1, starsColor = Color.White)

            Text(
                text = movie.getVoteFormatted(),
                fontSize = 14.sp,
                color = Color.White,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .align(Alignment.CenterVertically)
            )
        }

        GlideImage(
            model = movie.getPosterUrl(),
            contentDescription = "",
            modifier = Modifier
                .size(width = 140.dp, height = 240.dp)
                .align(Alignment.BottomStart)
                .padding(8.dp)

        )
    }
}

@Composable
private fun ChipGenres(genres: List<String>, modifier: Modifier) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
    ) {
        genres.forEach {
            Text(
                text = it,
                fontSize = 14.sp,
                color = Color.Black,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(end = 8.dp)
                    .clip(RoundedCornerShape(corner = CornerSize(12.dp)))
                    .background(Color.Gray)
                    .padding(12.dp)
            )
        }
    }
}


