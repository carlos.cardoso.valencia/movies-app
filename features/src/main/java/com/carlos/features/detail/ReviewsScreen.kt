package com.carlos.features.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.carlos.features.movies.EmptyState
import com.carlos.features.ui.BackArrow
import com.carlos.features.ui.Loader
import com.carlos.movies.model.ReviewMovieUIModel

@Composable
fun ReviewsScreen(
    movieId: String,
    viewModel: DetailMovieViewModel = hiltViewModel(),
    onBackCallback: () -> Unit,
) {

    LaunchedEffect(viewModel) {
        viewModel.getReviews(movieId)
    }

    val reviews by viewModel.reviews.collectAsStateWithLifecycle()
    val loading by viewModel.isLoading.collectAsStateWithLifecycle()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Gray)
    ) {

        BackArrow(onBackCallback)

        if (reviews.isEmpty() && !loading) {
            EmptyState()
        } else {
            LazyColumn(modifier = Modifier.padding(16.dp)) {
                items(reviews) { review ->
                    ReviewCard(review)
                }
            }
        }

        if (loading) {
            Loader()
        }


    }


}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
private fun ReviewCard(review: ReviewMovieUIModel) {

    Card(
        elevation = CardDefaults.cardElevation(4.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(horizontal = 4.dp, vertical = 8.dp)
            .fillMaxSize()
    ) {

        Row(modifier = Modifier.padding(4.dp)) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.weight(0.3f)
            ) {
                GlideImage(
                    model = review.getProfilepictureUrl(),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .padding(4.dp)
                        .size(60.dp)
                        .clip(CircleShape)
                        .border(2.dp, Color.White, CircleShape)
                )

                Text(
                    text = review.authorName,
                    fontSize = 14.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(vertical = 4.dp)
                )
            }

            Text(
                text = review.content,
                fontSize = 12.sp,
                color = Color.Black,
                maxLines = 7,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .padding(horizontal = 4.dp)
                    .weight(0.7f)

            )
        }

    }

}
