package com.carlos.features.image

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.carlos.features.R
import com.carlos.features.movies.EmptyState
import com.carlos.features.ui.Loader

@Composable
fun ImagesScreen(viewModel: ImagesViewModel = hiltViewModel()) {
    var selectedImageUris by remember {
        mutableStateOf<List<Uri>>(emptyList())
    }

    val isLoading by viewModel.isLoading.collectAsStateWithLifecycle()
    val uploadImages by viewModel.uploadImages.collectAsStateWithLifecycle()
    val imagesUrl by viewModel.imagesURL.collectAsStateWithLifecycle()
    val context = LocalContext.current

    LaunchedEffect(viewModel) {
        viewModel.getImages()
    }

    val multiplePhotoPickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.PickMultipleVisualMedia(),
        onResult = { uris -> selectedImageUris = uris }
    )

    selectedImageUris.forEach {
        LaunchedEffect(viewModel) {
            val bitmap = getBitmap(it, context)
            viewModel.uploadImage(bitmap)
        }
    }

    Box {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)

        ) {
            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                modifier = Modifier
                    .weight(0.9f)
            ) {
                items(imagesUrl) { uri ->
                    AsyncImage(
                        model = uri,
                        contentDescription = null,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(2.dp)
                            .size(150.dp),
                        contentScale = ContentScale.Crop
                    )
                }
            }

            Button(modifier = Modifier
                .weight(0.1f)
                .height(40.dp)
                .padding(16.dp)
                .fillMaxWidth(),
                enabled = !uploadImages,
                onClick = {
                    multiplePhotoPickerLauncher.launch(
                        PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly)
                    )
                }
            ) {
                val text = if (uploadImages) {
                    stringResource(R.string.label_images_upload_progress)
                } else {
                    stringResource(R.string.label_button_upload_image)
                }

                Text(text = text)
            }
        }

        if (isLoading) {
            Loader()
        }

        if (imagesUrl.isEmpty()) {
            EmptyState()
        }
    }


}

private fun getBitmap(imageUri: Uri, context: Context): Bitmap {
    return if (Build.VERSION.SDK_INT < 28) {
        MediaStore.Images
            .Media.getBitmap(context.contentResolver, imageUri)

    } else {
        val source = ImageDecoder
            .createSource(context.contentResolver, imageUri)
        ImageDecoder.decodeBitmap(source)
    }


}