package com.carlos.features.image

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carlos.images.repository.ImagesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class ImagesViewModel @Inject constructor(private val repo: ImagesRepository) : ViewModel() {

    val uploadImageResult = MutableStateFlow(false)
    val isLoading = MutableStateFlow(false)
    val uploadImages = MutableStateFlow(false)
    val imagesURL = MutableStateFlow<List<String>>(emptyList())
    fun getImages() {
        viewModelScope.launch {
            isLoading.value = true
            repo.getImages().collect { urlList ->
                isLoading.value = false
                imagesURL.value = urlList
            }
        }
    }

    fun uploadImage(bitmap: Bitmap) {
        isLoading.value = true
        uploadImages.value = true

        viewModelScope.launch {
            repo.uploadImage(bitmap).collect {
                uploadImageResult.value = it
                isLoading.value = false
                uploadImages.value = false

            }
        }

    }

}
