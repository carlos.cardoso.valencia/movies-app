package com.carlos.features.location

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.carlos.features.R
import com.carlos.features.movies.EmptyState
import com.carlos.location.model.LocationUIModel
import java.util.Date


@Composable
fun LocationScreen(viewModel: LocationViewModel = hiltViewModel()) {
    val locations by viewModel.locations.collectAsStateWithLifecycle()

    LaunchedEffect(viewModel) {
        viewModel.getLocations()
    }

    AskForLocation(LocalContext.current) { location ->
        viewModel.saveLocation(location)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {

        if (locations.isNotEmpty()) {
            MapLocation(
                locations, modifier = Modifier
                    .height(400.dp)
                    .padding(4.dp)
                    .clip(
                        RoundedCornerShape(8.dp)
                    )
            )

            LazyColumn {
                items(locations) { location ->
                    LocationsCard(location)
                }
            }
        } else {
            EmptyState()
        }

    }

}


@Composable
private fun AskForLocation(
    context: Context,
    locationCallback: (location: LocationUIModel) -> Unit
) {
    val permissions = mutableListOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    ).apply {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            add(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    val locationPermissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { permissionsMap ->
            val areGranted = permissionsMap.values.reduce { acc, next -> acc && next }
            if (areGranted) {
                startLocationService(context, locationCallback)
            } else {

            }
        })

    LaunchedEffect(locationPermissionLauncher) {
        locationPermissionLauncher.launch(permissions.toTypedArray())
    }
}

private fun startLocationService(
    context: Context,
    locationCallback: (location: LocationUIModel) -> Unit
) {


    if (!context.isMyServiceRunning(LocationService::class.java)) {
        Intent(context, LocationService::class.java).apply {
            action = LocationService.ACTION_START
            context.startService(this)
        }

        LocationService.callback = { lat, lng ->
            locationCallback.invoke(LocationUIModel(lat, lng, Date()))
        }

    }

}

@Suppress("DEPRECATION")
private fun Context.isMyServiceRunning(serviceClass: Class<*>): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
    for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
        if (serviceClass.name == service.service.className) {
            return true
        }
    }
    return false
}

@Composable
fun LocationsCard(location: LocationUIModel) {

    Card(
        elevation = CardDefaults.cardElevation(4.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White),
        modifier = Modifier
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .fillMaxSize()
    ) {
        Row(modifier = Modifier.padding(8.dp)) {
            Image(
                painter = painterResource(id = R.drawable.ic_location),
                contentDescription = "",
                modifier = Modifier
                    .size(30.dp)
                    .align(Alignment.CenterVertically)
            )

            Column(modifier = Modifier.padding(16.dp)) {

                Text(
                    text = stringResource(R.string.label_date, location.formattedDate),
                    fontSize = 18.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                )

                Text(
                    text = stringResource(
                        R.string.label_location_latitude,
                        location.lat.toString()
                    ),
                    fontSize = 15.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(end = 8.dp)
                )

                Text(
                    text = stringResource(
                        R.string.label_location_longitude,
                        location.lng.toString()
                    ),
                    fontSize = 15.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(end = 8.dp)
                )


            }


        }

    }


}
