package com.carlos.features.location


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.carlos.features.R
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
class LocationService : Service() {

    private val serviceScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
    private lateinit var locationClient: LocationClient
    private val CHANNEL_ID = "1"
    private val CHANNEL_NAME = "channel"
    private val INTERVAL_REQUEST = 300000L // 5 minutes

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        locationClient = DefautLocationClient(
            this,
            LocationServices.getFusedLocationProviderClient(applicationContext)
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> start()
            ACTION_STOP -> stop()
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun stop() {
        stopForeground(true)
        stopSelf()
    }
    private fun start() {
        val notification = NotificationCompat.Builder(this, "location")
            .setContentTitle(getString(R.string.notif_location_title))
            .setContentText(getString(R.string.notif_location_content))
            .setChannelId(CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_empty_box)
            .setOngoing(true)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )

            notificationManager.createNotificationChannel(channel)
            notification.setChannelId(CHANNEL_ID)
        }


        locationClient.getLocationUpdates(INTERVAL_REQUEST)
            .catch { e ->
                Log.d("LocationService:e ", e.toString())
            }
            .onEach { location ->
                val lat = location.latitude
                val lng = location.longitude
                val updatedNotification =
                    notification
                        .setSmallIcon(R.drawable.ic_location)
                        .setContentText(getString(R.string.label_notification_lat_lon, lat.toString(), lng.toString()))
                        .setChannelId(CHANNEL_ID)

                callback?.invoke(lat, lng)

                notificationManager.notify(1, updatedNotification.build())
            }.launchIn(serviceScope)


        startForeground(1, notification.build())
    }

    override fun onDestroy() {
        super.onDestroy()

        serviceScope.cancel()
    }

    companion object {
        var callback: ((lat: Double, lng: Double) -> Unit)? = null

        const val ACTION_START = "action_start"
        const val ACTION_STOP = "action_stop"

    }

}