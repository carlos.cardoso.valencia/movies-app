package com.carlos.features.location

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carlos.location.model.LocationUIModel
import com.carlos.location.repository.LocationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LocationViewModel @Inject constructor(private val repo: LocationRepository) : ViewModel() {

    val isLoading = MutableStateFlow(false)

    private val _locations = MutableStateFlow<List<LocationUIModel>>(emptyList())
    val locations: StateFlow<List<LocationUIModel>>
        get() = _locations

    fun saveLocation(location: LocationUIModel) {
        repo.saveLocation(location)
    }

    fun getLocations() {
        viewModelScope.launch {
            isLoading.emit(true)

            withContext(Dispatchers.IO) {
                repo.getLocations().collect {
                    _locations.value = it
                }

                isLoading.emit(false)
            }
        }

    }

}