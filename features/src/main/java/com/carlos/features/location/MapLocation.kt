package com.carlos.features.location

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.carlos.location.model.LocationUIModel
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState

@Composable
fun MapLocation(
    locations: List<LocationUIModel>,
    zoom: Float = 5f,
    modifier: Modifier = Modifier
) {
    val lastLocation = locations.last()
    val location = LatLng(lastLocation.lat, lastLocation.lng)
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(location, zoom)
    }

    Box {
        GoogleMap(
            modifier = modifier.fillMaxWidth(),
            cameraPositionState = cameraPositionState
        ) {
            locations.forEach {
                Marker(
                    state = MarkerState(position = LatLng(it.lat, it.lng)),
                    title = it.formattedDate.toString(),
                    snippet = ""
                )
            }

        }
    }
}