package com.carlos.features.movies

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.carlos.movies.model.MovieUIModel

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MoviesCard(
    movie: MovieUIModel,
    onItemClick: (String) -> Unit
) {

    Box(modifier = Modifier
        .padding(horizontal = 8.dp)
        .clickable { onItemClick.invoke(movie.id) }
    ) {

        GlideImage(
            model = movie.getPosterUrl(),
            contentDescription = "",
            modifier = Modifier.clip(RoundedCornerShape(4.dp))
        )


    }


}