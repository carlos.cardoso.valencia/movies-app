package com.carlos.features.movies

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.carlos.features.R
import com.carlos.features.ui.Loader
import com.carlos.movies.model.MovieUIModel

@Composable
fun MoviesScreen(
    viewModel: MoviesViewModel = hiltViewModel(),
    onItemClick: (String) -> Unit
) {

    LaunchedEffect(viewModel) {
        viewModel.getMovies()
    }

    val isLoading: Boolean by viewModel.isLoading.collectAsStateWithLifecycle()
    val popularMovies by viewModel.popularMovies.collectAsStateWithLifecycle()
    val topRatedMovies by viewModel.topRatedMovies.collectAsStateWithLifecycle()
    val recommendedMovies by viewModel.recommendedMovies.collectAsStateWithLifecycle()

    Box(
        modifier = Modifier
            .background(Color.Gray)
            .fillMaxSize()
    ) {

        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
        ) {
            if (popularMovies.isNotEmpty()) {
                MoviesList(popularMovies, stringResource(R.string.label_movies_popular), onItemClick)
                MoviesList(recommendedMovies, stringResource(R.string.label_movies_recommendations), onItemClick)
                MoviesList(topRatedMovies, stringResource(R.string.label_movies_top_rated), onItemClick)
            }
        }

        if (popularMovies.isEmpty() && !isLoading) {
            EmptyState()
        }

        if (isLoading) {
            Loader()
        }
    }


}


@Composable
fun MoviesList(
    movies: List<MovieUIModel>,
    header: String,
    onItemClick: (String) -> Unit
) {

    Column(
    ) {
        Text(
            text = header,
            fontSize = 22.sp,
            fontWeight = FontWeight.Bold,
            color = Color.White,
            modifier = Modifier.padding(8.dp)
        )


        LazyRow(modifier = Modifier.run { padding(vertical = 8.dp) }) {
            items(movies) { movie ->
                MoviesCard(movie, onItemClick)
            }
        }
    }


}



