package com.carlos.features.movies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carlos.movies.map.map
import com.carlos.movies.map.toRoomModel
import com.carlos.movies.model.MovieListType
import com.carlos.movies.model.MovieUIModel
import com.carlos.movies.repository.LocalMoviesRepository
import com.carlos.movies.repository.RemoteMoviesRepository
import com.carlos.network.NetworkCall
import com.carlos.network.model.MoviesResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val remoteRepository: RemoteMoviesRepository,
    private val localRepository: LocalMoviesRepository
) : ViewModel() {

    val isLoading = MutableStateFlow(false)

    val popularMovies: MutableStateFlow<List<MovieUIModel>> = MutableStateFlow(emptyList())
    val topRatedMovies: MutableStateFlow<List<MovieUIModel>> = MutableStateFlow(emptyList())
    val recommendedMovies: MutableStateFlow<List<MovieUIModel>> = MutableStateFlow(emptyList())

    /** */
    fun getMovies() {
        viewModelScope.launch {
            isLoading.emit(true)

            val responseTopRated = remoteRepository.getTopRatedMovies()
            val responseRecommended = remoteRepository.getRecommendatedMovies()

            randomPopularMovies(5000L)

            responseRecommended.handleMoviesResponse(recommendedMovies, MovieListType.Recommendation)
            responseTopRated.handleMoviesResponse(topRatedMovies, MovieListType.TopRated)
        }
    }

    private suspend fun randomPopularMovies(timeInterval: Long): Job {
        return CoroutineScope(Dispatchers.Default).launch {
            while (NonCancellable.isActive) {
                getPopularMoviesByRandomPage()
                delay(timeInterval)
            }
        }
    }

    private suspend fun getPopularMoviesByRandomPage() {
        val randomPage = (0..10).random()
        val responsePopular = remoteRepository.getPopularMovies(randomPage)
        responsePopular.handleMoviesResponse(popularMovies, MovieListType.Popular)
    }

    private suspend fun NetworkCall.Response<MoviesResponse>.handleMoviesResponse(
        moviesFlow: MutableStateFlow<List<MovieUIModel>>,
        movieType: MovieListType
    ) {

        when (this) {
            is NetworkCall.Response.GenericError, is NetworkCall.Response.NetworkError -> {
                isLoading.emit(false)
                getMoviesFromDb(moviesFlow, movieType)
            }

            is NetworkCall.Response.Success -> {
                isLoading.emit(false)
                val movies = data.movies?.map()

                moviesFlow.emit(data.movies?.map() ?: emptyList())
                saveMoviesIntoDB(movies, movieType)
            }
        }
    }

    private suspend fun getMoviesFromDb(
        moviesFlow: MutableStateFlow<List<MovieUIModel>>,
        movieType: MovieListType
    ) {

        with(localRepository) {
            val movies = when (movieType) {
                MovieListType.Popular -> getPopularMovies()
                MovieListType.TopRated -> getTopRatedMovies()
                MovieListType.Recommendation -> getRecommendatedMovies()
            }

            moviesFlow.emit(movies.distinctBy { it.title })

        }

    }

    private suspend fun saveMoviesIntoDB(movies: List<MovieUIModel>?, type: MovieListType) {
        movies?.forEach {
            val movieRoomModel = it.toRoomModel().apply {
                this.type = type.toString()
            }
            localRepository.insertMovie(movieRoomModel)
        }


    }

}

