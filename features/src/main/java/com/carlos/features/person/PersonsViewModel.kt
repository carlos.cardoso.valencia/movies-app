package com.carlos.features.person

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.carlos.movies.map.mapPersonsFromNetwork
import com.carlos.movies.model.PersonUIModel
import com.carlos.movies.repository.PersonsRepository
import com.carlos.network.NetworkCall
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PersonsViewModel @Inject constructor(
    private val repository: PersonsRepository,
) : ViewModel() {

    val popularPersons: MutableStateFlow<List<PersonUIModel>> = MutableStateFlow(emptyList())
    val isLoading = MutableStateFlow(false)

    fun getPersons() {
        isLoading.value = true

        viewModelScope.launch {
            val response = repository.getPopularPersons()
            when (response) {

                is NetworkCall.Response.GenericError, NetworkCall.Response.NetworkError -> {
                    isLoading.value = false
                }

                is NetworkCall.Response.Success -> {
                    isLoading.value = false
                    popularPersons.value = response.data.results.mapPersonsFromNetwork()
                }

            }

        }
    }


}