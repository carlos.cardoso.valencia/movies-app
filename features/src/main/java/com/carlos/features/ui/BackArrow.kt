package com.carlos.features.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBackIos
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun BackArrow(
    onBack: (() -> Unit)? = null,
    modifier: Modifier = Modifier
) {
    IconButton(
        onClick = {
            onBack?.invoke()
        }, modifier = modifier
            .padding(4.dp)
    ) {
        Icon(Icons.Rounded.ArrowBackIos, "", tint = Color.White)
    }
}