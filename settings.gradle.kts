pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Movies"
include(":app")
include(":data:movies")
include(":core:network")
include(":features")
include(":data:location")
include(":core:firebase")
include(":data:images")
